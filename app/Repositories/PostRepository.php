<?php

namespace App\Repositories;

use App\Post;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class PostRepository.
 */
class PostRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Post::class;
    }

    /**
     * ngambil data postingan yang terpaginasi
     *
     * @param [type] $search
     * @param [type] $perPage
     * @return void
     */
    public function getListPaginated($search, $perPage)
    {
        return $this->model::where(function ($q) use ($search) {
            if (!empty($search)) {
                # gua nyari data berdasarkan title
                $q->where('title', 'like', "%$search%");
            }
        })->paginate($perPage);
    }

    /**
     * Simpen data postingan
     *
     * @param array $data
     * @return void
     */
    public function store(array $data)
    {
        return $this->model::create($data);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param [type] $data
     * @return void
     */
    public function update(Post $post, array $data)
    {
        $post->update($data);
        $post->save();
        return $post;
    }
}
